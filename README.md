### UI Automation for Native calendar android app
 * Framework: (Appium , Java , Cucumber, BDD , POM)
 
### Project structure
 * All the neccessary dependencies are loaded in pom.xml ( Includes : Appium,cucumber,java nad mvn)
 * Features files which inculdes Given, When and Then is under src/test/java
 * Corosponding stepdef(glue) is under Steps folder
 * Locators are under src/main/java/ - page.locators folder
 * Automation Actions such as click,isDisplayed is captured under page.actions folder in again src/main/java
 * Automation util is where device capabalities is captured along with the Android get driver
 * Note: was also trying to build the config so all the dependencies are not hardcoded in java file, so introduced resources
 
### Pre-Req to run
 * Install Java
 * Install Android studio (basic sdk manager is sufficient) just to capture and enabled adb properties and load emualtor
 * Install maven
 * Install Appium
 * Now get all the javahome,mvnhome and sdk tools configure in the environment variables depending on mac or win OS users
 * Install TestNG plugin 
 * Have an IDE
 
### To Run
 * Clone the repo
 * Open the repo in your IDE
 * Do a mvn build from cmd or use the IDE so that all the dependdencies are loaded
 * Now in the AutomaionUtils.java file which is in src/main/java/utils folder. 
   Make sure you change the deviceName,platformVersion depending on your machine/emulator/usbdevice
 * To get that you can use adb devices for the deviceName and OS from the deviceInfo
 * Now using the RunCucke.Java run this project using TestNG runner(run as testng if not auto popped up use the run config).
 
### What works ?
 * It compiles without any error
 * Launches the calendar app on your emulator android device
 * Open the main activity
 * Clicks on the addBtn
 * Stops there !
  
### Known issue
 * This project / Repo does not fully work, it is partiall implementation
 * The calendar app at certain point need to switch to frame and continue on implementation
 
### What can be done?
 * Need to implment the frame switch
 * Then continue on the rest of the scenario to finish the automation
 * More improvements on the framework can be done.

### To add the existing feature


