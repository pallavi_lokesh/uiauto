package utils;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebElement;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.remote.MobileCapabilityType;


public class AutomationUtils {
	public static AndroidDriver<MobileElement> Mobiledriver;
	static String URL;
	public static void installApp() throws MalformedURLException {
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("noReset", true);
		capabilities.setCapability(MobileCapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, true);
		capabilities.setCapability(MobileCapabilityType.VERSION, "1.6.5");
		capabilities.setCapability("browserName", "");
		capabilities.setCapability("platformName", "Android");
	    capabilities.setCapability("deviceName", "emulator-5554");//R58M47TSC4M //2b64407d891c7ece //emulator-5554
		capabilities.setCapability("platformVersion", "8.0");
		capabilities.setCapability("appPackage", "com.google.android.calendar");
		capabilities.setCapability("appActivity", "com.android.calendar.AllInOneActivity");
		capabilities.setCapability("browserstack.debug", true);
		capabilities.setCapability("autoAcceptAlerts", true);
		capabilities.setCapability("autoDismissAlerts", true);
	    Mobiledriver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
	 	Mobiledriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);	
	}
	
	public static AndroidDriver<MobileElement> getDriver() {
		return Mobiledriver;
	}

	public static void tearDownClose() {
		if (Mobiledriver != null) {
			Mobiledriver.closeApp();
			System.out.println("driver.close()");	
		}
	}

}
