package page.actions;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import page.locators.calenderApp;
import page.locators.calenderApp;
import utils.AutomationUtils;;

public class automationAction {

	public static AndroidDriver<MobileElement> driver;
	calenderApp calenderApp = new calenderApp();
	
	public automationAction() {
		this.calenderApp = new calenderApp();
		PageFactory.initElements(new AppiumFieldDecorator(AutomationUtils.getDriver()), calenderApp);
	}
	
	public void actionBtnTap() {
		try {
			calenderApp.ActionBtn.click();
			System.out.println("Element is Tap");
		} catch (NoSuchElementException e) {
			System.out.println("Element not found for ActionBtn");
		}
	}
	
	public void actionBtnDis() {
		try {
			calenderApp.ActionBtn.isDisplayed();
			System.out.println("Element is Dispalyed");
		} catch (NoSuchElementException e) {
			System.out.println("Element not found for ActionBtn");
		}
	}
	
	public void eventBtnTap() {
		try {
			calenderApp.eventBtn.click();
			System.out.println("Element is Tap");
		} catch (NoSuchElementException e) {
			System.out.println("Element not found for eventBtn");
		}
	}
	
	
}
