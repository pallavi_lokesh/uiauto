package page.locators;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class calenderApp {

	@AndroidFindBy(id = "com.google.android.calendar:id/floating_action_button")
	public MobileElement ActionBtn;
	
	@AndroidFindBy(id = "com.google.android.calendar:id/speed_dial_event_container")
	public MobileElement eventBtn;
	
	@AndroidFindBy(id = "com.google.android.calendar:id/title")
	public MobileElement addTitleField;
	
	//com.google.android.calendar:id/floating_action_button
    //android.view.View[@content-desc="May 2020"]

}
