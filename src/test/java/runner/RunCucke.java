package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = { "src/test/java" }, 
                 glue = { "Steps" }, 
                 monochrome = true, 
                 tags = {"@cal"})
                		
public class RunCucke extends AbstractTestNGCucumberTests {
	
}