package Steps;

import java.net.MalformedURLException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeSuite;
import cucumber.api.java.Before;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import utils.AutomationUtils;;

public class BeforeActions {

	public static AndroidDriver<MobileElement> Mobiledriver;
	DesiredCapabilities capabilities = new DesiredCapabilities();
	
	@Before
	public static void setup() throws MalformedURLException {
		System.out.println("Launch the App-test");	
		AutomationUtils.installApp();
	}
	
}