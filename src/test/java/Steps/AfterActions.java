package Steps;

import utils.AutomationUtils;
import org.openqa.selenium.remote.DesiredCapabilities;
import cucumber.api.java.After;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.MobileElement;

public class AfterActions {
	
	public AndroidDriver<MobileElement> Mobiledriver;
	DesiredCapabilities capabilities = new DesiredCapabilities();
	
	@After
	public void tearDown() {
		System.out.println("driver.quit()");
		AutomationUtils.tearDownClose();
	}
}
