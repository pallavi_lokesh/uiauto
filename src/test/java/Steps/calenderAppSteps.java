package Steps;

import org.openqa.selenium.By;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import page.actions.automationAction;
import page.locators.calenderApp;

public class calenderAppSteps {

	automationAction automationAction = new automationAction();
	calenderApp calenderApp = new calenderApp();
	
	public static AndroidDriver<MobileElement> Mobiledriver;

	@Given("^I have launch the cal app$")
	public void I_have_launch_the_call_app() throws Throwable {
		System.out.println("====================");
		System.out.println("App is launched");
		automationAction.actionBtnDis();
	}
	
	@When("^It is not a weekend$")
	public void It_is_not_a_weekend() throws Throwable {
		automationAction.actionBtnTap();
		//	Have to switch to frames here 
		//	Needs more work
		
		//		Mobiledriver.context("WEBVIEW");
		//		Mobiledriver.switchTo().frame(1);
		
       //		automationAction.eventBtnTap();
	}
	
	@And("^It is not a public holiday$")
	public void It_is_not_a_public_holiday() throws Throwable {
		//      To implement
		//		calenderApp.addTitleField.sendKeys("Introduction");
	}
}
